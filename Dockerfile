FROM python:3.8.6-slim

WORKDIR /code

# Install required packages
COPY requirements.txt .
RUN pip install -r requirements.txt

# Copy the script
COPY src/ .

CMD [ "python", "./main.py" ]
