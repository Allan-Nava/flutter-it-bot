import logging
import os
import random
import re

from telegram import Update
from telegram.ext import (
    CallbackContext,
    CommandHandler,
    Filters,
    MessageHandler,
    Updater,
)

import scraper

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__name__)


def start(update: Update, context: CallbackContext) -> None:
    update.message.reply_text("Ciao Dev")


def articles(update: Update, context: CallbackContext) -> None:
    msg = ""
    articles = scraper.articles()

    for article in articles:
        msg += "\n".join(
            [
                f"***{article['title']}***",
                f"{article['summary']}",
                f"[Leggi tutto]({article['link']})",
                "\n",
            ]
        )

    update.message.reply_markdown(msg)


def dio(update: Update, context: CallbackContext) -> None:
    # Get stickers filenames in sticker folder
    stickers = os.listdir("stickers/")
    # Get the absolute path a random sticker
    dio = os.path.abspath("stickers/" + random.choice(stickers))

    update.message.reply_sticker(open(dio, "rb"))


def main():
    """ Run bot """

    # Read token from parameters
    token = str(os.environ["TOKEN"])

    updater = Updater(token)
    dp = updater.dispatcher

    # List of commands available
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("articoli", articles))

    # Detect if dio or dart:dio is written in messages
    dp.add_handler(
        MessageHandler(
            Filters.regex(re.compile(r"\b(dart:)?dio\b", re.IGNORECASE)), dio
        )
    )

    updater.start_polling()
    updater.idle()


if __name__ == "__main__":
    main()
