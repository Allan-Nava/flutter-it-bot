import logging
import re

import requests
from bs4 import BeautifulSoup

logger = logging.getLogger(__name__)


def articles() -> list:
    """
    Fetch the articles of flutteritalia.dev

    Returns: a list of dictionaries (keys: title, summary, link)
    """
    # Get the HTML page
    html = requests.get("https://www.flutteritalia.dev/300f0-web-agency-news/").text
    soup = BeautifulSoup(html, "html.parser")
    # Find all the tags named article
    articles = soup.find_all("article")

    result = list()

    # Since the script uses the page with all posts,
    # we can only read the title, summary, the author, when (s)he published the article and the "continue reading" link
    for preview in articles:
        title = preview.find("h2")
        thumbnail = preview.find(attrs={"class": re.compile(r".*\bentry-summary\b.*")})
        # The space before "Leggi" is necessary
        summary = thumbnail.text.split("Leggi tutto")[0]
        # "Read more..." link
        link = thumbnail.a["href"]

        result.append(
            {
                "title": title.text,
                "summary": summary,
                "link": link,
            }
        )

        logger.info(f"Read article -> title={title.text} summary={summary} link={link}")

    return result
